import sqlite3
from typing import Optional

import interactions as inter

from locals import LWI, embed_pas_lwi, EMOJI, ID_EXEC, ID_INPUT_EXEC
from squelette import Squelette


class CommandesLwi(Squelette):
    @inter.extension_command(name='backup',
                             description='Crée une sauvegarde de la base de données. Pour Lwi uniquement.')
    async def backup(self, ctx: inter.CommandContext) -> None:
        if int(ctx.author.user.id) != LWI:
            await ctx.send(embeds=embed_pas_lwi(), ephemeral=True)
            return
        self.gbd.creer_backup()
        embed_backup = inter.Embed(
            title=f"{EMOJI.name} Sauvegarde effectuée",
            description='La base de données a été sauvegardée avec succès.',
            color=inter.Color.green()
        )
        if self.logger is not None:
            self.logger.info("Backup créée.")
        await ctx.send(embeds=embed_backup, ephemeral=True)

    @inter.extension_command(name='logs',
                             description='Envoie le fichier de logs en dm. Pour Lwi uniquement.')
    async def logs(self, ctx: inter.CommandContext):
        if int(ctx.author.user.id) != LWI:
            await ctx.send(embeds=embed_pas_lwi(), ephemeral=True)
            return
        await ctx.author.user.send(files=inter.File('logs.txt'))
        embed_logs = inter.Embed(
            title=f'{EMOJI.name} Logs envoyés',
            description="Tu devrais avoir reçu le fichier *logs.txt* en message privé.",
            color=inter.Color.fuchsia())
        await ctx.send(embeds=embed_logs, ephemeral=True)

    @inter.extension_command(name='exec',
                             description='Execute une requête SQL. Pour Lwi uniquement.')
    async def exec_sql(self, ctx: inter.CommandContext):
        if int(ctx.author.user.id) != LWI:
            await ctx.send(embeds=embed_pas_lwi(), ephemeral=True)
            return
        modal = inter.Modal(
            title=f'{EMOJI.name} Execution de requête SQL',
            custom_id=ID_EXEC,
            components=[inter.TextInput(
                style=inter.TextStyleType.PARAGRAPH,
                custom_id=ID_INPUT_EXEC,
                min_length=1,
                max_length=500,
                label="Écrivez la requête SQL"
            )]
        )
        await ctx.popup(modal)

    @inter.extension_modal(ID_EXEC)
    async def exec_recu(self, ctx: inter.ComponentContext, reponse: str):
        if self.logger is not None:
            self.logger.info(f"Exécution de la requête {reponse}")
        try:
            res = self.gbd.executer_requete(reponse).fetchall()
        except sqlite3.OperationalError as e:
            if self.logger is not None:
                self.logger.error("Erreur dans /exec", exc_info=True)
            res = f'{type(e)} : {e}'
        else:
            if self.logger is not None:
                self.logger.info(f"Résultat de la requête : {res}")
        embed = inter.Embed(
            title=f"{EMOJI.name} Résultat d'exécution",
            description=f'Requête : `{reponse}`',
            color=inter.Color.fuchsia(),
            fields=[inter.EmbedField(
                name='Retour de la commande',
                value=str(res)
            )]
        )
        await ctx.send(embeds=embed, ephemeral=True)

    @inter.extension_command(
        name='supprimer_gobelet',
        description='Supprime le dernier gobelet. Pour Lwi uniquement.',
        options=[
            inter.Option(
                name='personne_cible',
                description='La personne dont il faut supprimer un gobelet.',
                type=inter.OptionType.USER,
                required=False
            ),
            inter.Option(
                name='boisson',
                description='L\'id de la boisson spécifique.',
                type=inter.OptionType.INTEGER,
                required=False
            ),
            inter.Option(
                name='taille',
                description='La taille du gobelet',
                type=inter.OptionType.BOOLEAN,
                required=False
            ),
            inter.Option(
                name='date',
                description='La date',
                type=inter.OptionType.STRING,
                required=False
            )
        ]
    )
    async def supprimer_gobelet(self, ctx: inter.CommandContext, personne_cible: Optional[inter.User] = None,
                                boisson: Optional[int] = None, taille: Optional[bool] = None, date: str = ''):
        if int(ctx.author.user.id) != LWI:
            await ctx.send(embeds=embed_pas_lwi(), ephemeral=True)
            return
        if personne_cible is None:
            id_bd = None
        else:
            id_bd = self.gbd.assoiffe_par_discord(int(personne_cible.id))
        self.gbd.supprimer_dernier(id_bd, boisson, taille, date)
        await ctx.send("Flemme de faire joli", ephemeral=True)
