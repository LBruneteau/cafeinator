import enum
from typing import Optional

import interactions as inter
from interactions.ext.wait_for import wait_for_component

from locals import EMOJI, est_date_valide, embed_erreur_date, generer_select_taille, ID_ENVOYER, embed_pas_concerne, \
    ID_CHOIX_BOISSON, ID_SELECT_TAILLE, embed_temps_ecoule, embed_traitement, get_nom_taille, generer_select_personne, \
    ID_SELECT_PERSONNE, embed_choisir_personne, embed_pas_inscrit
from squelette import Squelette

ModeLB = enum.Enum('ModeLB', ['GOB', 'PRIX'])


class Classement(Squelette):

    async def classement(self, ctx: inter.CommandContext, date: str, mode: ModeLB):
        embed_form = inter.Embed(
            title=f"{EMOJI.name} Classement des assoiffés",
            color=inter.Color.white(),
            description='Vous pouvez rejeter tous les messages "*Caféinator réfléchit...*"'
        )
        nom_func = 'Classement gob' if mode == ModeLB.GOB else 'Classement prix'
        if self.logger is not None:
            self.logger.info(f'{ctx.author.user.username} appelle {nom_func}')

        if not est_date_valide(date):
            await ctx.send(embeds=embed_erreur_date(date), ephemeral=True)
            return

        choix_boisson = self.genere_choix_boisson(False)
        choix_taille = generer_select_taille(False)
        bouton_submit = inter.Button(style=inter.ButtonStyle.PRIMARY, label='Envoyer', custom_id=ID_ENVOYER)
        id_boisson = None
        taille = None
        submit_clique = False

        liste_composants = [[choix_boisson], [choix_taille], [bouton_submit]]

        async def check(comp_ctx: inter.ComponentContext) -> bool:
            nonlocal id_boisson, taille, submit_clique
            if int(comp_ctx.author.user.id) != int(ctx.author.user.id):
                await ctx.send(embeds=embed_pas_concerne(), ephemeral=True)
                return False
            if comp_ctx.data.custom_id == ID_ENVOYER:
                submit_clique = True
            elif comp_ctx.data.custom_id == ID_CHOIX_BOISSON:
                id_boisson = None if int(comp_ctx.data.values[0]) <= 0 else int(comp_ctx.data.values[0])
            elif comp_ctx.data.custom_id == ID_SELECT_TAILLE:
                taille = int(comp_ctx.data.values[0]) if len(comp_ctx.data.values) == 1 else None
            return True

        await ctx.send(embeds=embed_form, components=liste_composants, ephemeral=True)

        while True:
            try:
                ctx_btn: inter.ComponentContext = await wait_for_component(self.bot,
                                                                           components=[choix_boisson, choix_taille,
                                                                                       bouton_submit],
                                                                           check=check, timeout=45)
            except TimeoutError:
                await ctx.edit(embeds=embed_temps_ecoule(), components=[])
                if self.logger is not None:
                    self.logger.info("Requête Classement expirée")
                return
            else:
                if submit_clique:
                    break
                await ctx_btn.defer(ephemeral=True)
        await ctx.edit(embeds=embed_traitement(), components=list())

        if self.logger is not None:
            self.logger.info(f"Appel de {nom_func}({id_boisson}, {taille}, '{date}')")
        if mode == ModeLB.GOB:
            res = self.gbd.classement_assoiffes(id_boisson, taille, date)
        else:
            res = self.gbd.classement_assoiffes_prix(id_boisson, taille, date)

        if self.logger is not None:
            self.logger.info(f"{nom_func} retourne {res}")

        embed_final = inter.Embed(
            title=f"{EMOJI.name} Classement des assoiffés",
            color=inter.Color.green()
        )
        liste_noms_id = self.gbd.liste_noms_id()
        for i, (id_bd, nb_gob) in enumerate(res):
            if i > 9:
                break
            sous_liste = [i for i in liste_noms_id if i[0] == id_bd][0]
            titre = f'{i + 1} : {sous_liste[1]} {sous_liste[2].upper()}'
            if mode == ModeLB.GOB:
                val = f"**{nb_gob}** gobelets"
            else:
                val = f"**{nb_gob:.2f}€ dépensés**"
            embed_final.add_field(titre, val)

        fields = [
                inter.EmbedField(
                    name="Boisson choisie",
                    value="Toutes les boissons" if id_boisson is None else self.gbd.nom_boisson_par_id(id_boisson)),
                inter.EmbedField(
                    name="Taille des gobelets",
                    value=str(get_nom_taille(taille)).title() if taille is not None else 'Toutes'
                ),
                inter.EmbedField(
                    name="Date spécifiée",
                    value=date if date else 'Aucune'
                )]
        for f in fields:
            embed_final.add_field(f.name, f.value)
        await ctx_btn.send(embeds=embed_final)

    @inter.extension_command(
        name='classement_gobelets',
        description='Fais un top 10 des assoiffés ayant donné le plus de gobelets.',
        options=[inter.Option(
            name='date',
            description='La date précise au format YYYY-MM-DD, supporte >, < et /',
            type=inter.OptionType.STRING,
            required=False)]
    )
    async def classement_gobelets(self, ctx: inter.CommandContext, date: str = ''):
        await self.classement(ctx, date, ModeLB.GOB)

    @inter.extension_command(
        name='classement_prix',
        description='Fais un top 10 des plus grands dépensiers de l\'IUT.',
        options=[inter.Option(
            name='date',
            description='La date précise au format YYYY-MM-DD, supporte >, < et /',
            type=inter.OptionType.STRING,
            required=False)]
    )
    async def classement_prix(self, ctx: inter.CommandContext, date: str = ''):
        await self.classement(ctx, date, ModeLB.PRIX)

    @inter.extension_command(name='classement_boissons',
                             description='Fais un top 10 des boissons les plus appréciées.',
                             options=[
                                 inter.Option(
                                     name="personne_cible",
                                     description="La personne dont vous voulez faire le top 10.",
                                     type=inter.OptionType.USER,
                                     required=False),
                                 inter.Option(
                                     name='date',
                                     description='La date précise au format YYYY-MM-DD, supporte >, < et /',
                                     type=inter.OptionType.STRING,
                                     required=False
                                 )
                             ]
                             )
    async def classement_boissons(self, ctx: inter.CommandContext,
                                  personne_cible: Optional[inter.User] = None, date: str = ''):
        embed_form = inter.Embed(
            title=f"{EMOJI.name} Classement des boissons",
            color=inter.Color.white(),
            description='Vous pouvez rejeter tous les messages "*Caféinator réfléchit...*"'
        )
        if self.logger is not None:
            self.logger.info(f'{ctx.author.user.username} appelle classement boissons')

        if not est_date_valide(date):
            await ctx.send(embeds=embed_erreur_date(date), ephemeral=True)
            return

        utilisateur: inter.User = personne_cible if personne_cible is not None else ctx.author.user
        choix_personne = generer_select_personne(f'{utilisateur.username}#{utilisateur.discriminator}')
        choix_taille = generer_select_taille(False)
        bouton_submit = inter.Button(style=inter.ButtonStyle.PRIMARY, label='Envoyer', custom_id=ID_ENVOYER)
        personne_choisie = -1
        taille = None
        submit_clique = False

        liste_composants = [[choix_personne], [choix_taille], [bouton_submit]]

        async def check(comp_ctx: inter.ComponentContext) -> bool:
            nonlocal personne_choisie, taille, submit_clique
            if int(comp_ctx.author.user.id) != int(ctx.author.user.id):
                await ctx.send(embeds=embed_pas_concerne(), ephemeral=True)
                return False
            if comp_ctx.data.custom_id == ID_ENVOYER:
                if personne_choisie == -1:
                    await comp_ctx.send(embeds=embed_choisir_personne(), ephemeral=True)
                    return False
                submit_clique = True
            elif comp_ctx.data.custom_id == ID_SELECT_PERSONNE:
                personne_choisie = int(comp_ctx.data.values[0])
            elif comp_ctx.data.custom_id == ID_SELECT_TAILLE:
                taille = int(comp_ctx.data.values[0]) if len(comp_ctx.data.values) == 1 else None
            return True

        await ctx.send(embeds=embed_form, components=liste_composants, ephemeral=True)

        while True:
            try:
                ctx_btn: inter.ComponentContext = await wait_for_component(self.bot,
                                                                           components=[choix_personne, choix_taille,
                                                                                       bouton_submit],
                                                                           check=check, timeout=45)
            except TimeoutError:
                await ctx.edit(embeds=embed_temps_ecoule(), components=[])
                if self.logger is not None:
                    self.logger.info("Requête Classement expirée")
                return
            else:
                if submit_clique:
                    break
                await ctx_btn.defer(ephemeral=True)
        await ctx.edit(embeds=embed_traitement(), components=list())

        id_bd = None
        if personne_choisie:
            try:
                id_bd = self.gbd.assoiffe_par_discord(int(utilisateur.id))
            except ValueError:
                await ctx_btn.send(embeds=embed_pas_inscrit(utilisateur))
                return

        if self.logger is not None:
            self.logger.info(f"Appel de classement boissons({id_bd}, {taille}, '{date}')")
        res = self.gbd.classement_boissons(id_bd, taille, date)

        if self.logger is not None:
            self.logger.info(f"classement boissons retourne {res}")

        embed_final = inter.Embed(
            title=f"{EMOJI.name} Classement des boissons",
            color=inter.Color.green()
        )
        for i, (nom_boisson, nb_gob) in enumerate(res):
            if i > 9:
                break
            titre = f'{i + 1} : {nom_boisson}'
            val = f"**{nb_gob}** gobelets"
            embed_final.add_field(titre, val)

        fields = [
                inter.EmbedField(
                    name="Personne ciblée",
                    value="Tous les assoiffés" if id_bd is None else utilisateur.mention),
                inter.EmbedField(
                    name="Taille des gobelets",
                    value=str(get_nom_taille(taille)).title() if taille is not None else 'Toutes'
                ),
                inter.EmbedField(
                    name="Date spécifiée",
                    value=date if date else 'Aucune'
                )]
        for f in fields:
            embed_final.add_field(f.name, f.value)
        await ctx_btn.send(embeds=embed_final)
