import interactions as inter
from typing import Optional
import logging
from interactions.ext.wait_for import setup
from gestionbd import GestionBd
from locals import EMOJI, ID_CHOIX_BOISSON


class Squelette(inter.Extension):

    def __init__(self, bot: inter.Client, chemin: str = 'sql/donnees_gros.db', logger: Optional[logging.Logger] = None):
        super().__init__()
        self.bot = bot
        setup(bot)
        self.gbd: GestionBd = GestionBd(chemin, logger)
        self.logger: Optional[logging.Logger] = logger

    def genere_choix_boisson(self, insertion: bool) -> inter.SelectMenu:
        texte_premier = 'Toutes les boissons' if not insertion else 'Boisson inconnue'
        premier = inter.SelectOption(label=texte_premier, value=-1, default=not insertion)
        options = [premier]
        options += [inter.SelectOption(label=nom, value=id_boisson, emoji=EMOJI)
                    for id_boisson, nom, _, _, _ in self.gbd.liste_boissons()]
        return inter.SelectMenu(options=options, custom_id=ID_CHOIX_BOISSON, placeholder='Choisissez une boisson')

    @inter.extension_command(name='ping', description='Répond pong pour vérifier si le bot est accessible.')
    async def ping(self, ctx: inter.CommandContext) -> None:
        if self.logger is not None:
            self.logger.info('Fonction ping appelée.')
        embed_pong = inter.Embed(
            title=f"{EMOJI.name} Pong",
            color=inter.Color.fuchsia()
        )
        await ctx.send(embeds=embed_pong)


