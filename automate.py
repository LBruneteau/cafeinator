from classement import Classement
from commandeslwi import CommandesLwi
from consultation import Consultation
from gobelet import Gobelet
from inscription import Inscription


class Automate(Consultation, CommandesLwi, Classement, Inscription, Gobelet):
    pass
