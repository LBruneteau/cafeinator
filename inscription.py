import asyncio

import interactions as inter
from interactions.ext.wait_for import wait_for_component

from locals import EMOJI, LWI, embed_pas_lwi, ID_CONFIRMER, ID_ANNULER, embed_pas_concerne, embed_temps_ecoule, \
    embed_traitement, embed_annule
from squelette import Squelette


class Inscription(Squelette):

    async def inscription_gens(self, ctx: inter.CommandContext, prenom: str, nom: str, discord: inter.User,
                               annee: int, groupe_td: int, groupe_tp: int):
        id_discord = int(discord.id)
        prenom, nom = prenom.title(), nom.title()
        annee = None if annee == 0 else annee
        groupe_td = None if groupe_td == 0 else groupe_td
        groupe_tp = None if groupe_tp == 0 else groupe_tp
        raison_invalidite = ''
        a_confirme = False
        if annee not in range(4) and annee is not None:
            raison_invalidite = 'L\'année n\'est pas correcte.'
        elif groupe_td not in range(6) and annee is not None:
            raison_invalidite = 'Le groupe de TD n\'est pas valable.'
        elif (groupe_tp not in range(3) or (groupe_td == 5 and groupe_tp is not None)) and annee is not None:
            raison_invalidite = 'Le groupe de TP n\'est pas valable.'
        else:
            try:
                self.gbd.assoiffe_par_discord(id_discord)
            except ValueError:
                pass
            else:
                raison_invalidite = 'Cette personne est déjà inscrite dans la base de données.'
        if raison_invalidite:
            embed_erreur = inter.Embed(
                title=f"{EMOJI.name} Erreur",
                description=raison_invalidite,
                color=inter.Color.red()
            )
            await ctx.send(embeds=embed_erreur, ephemeral=True)
            return
        groupe_iut = "Externe à l'IUT"
        if annee is not None:
            groupe_iut = f'{annee}A '
            groupe_iut += 'alternant' if groupe_td == 5 else f'groupe {groupe_td}-{groupe_tp}'
        embed_verif = inter.Embed(
            title=f'{EMOJI.name} Confirmation de l\'inscription',
            color=inter.Color.yellow(),
            description='Veuillez vérifier si les informations suivantes sont correctes',
            fields=[inter.EmbedField(
                name='Nom',
                value=f'{prenom} {nom.upper()}'),
                inter.EmbedField(
                    name='Discord',
                    value=discord.mention),
                inter.EmbedField(
                    name='Groupe',
                    value=groupe_iut)])
        bouton_confirmer = inter.Button(style=inter.ButtonStyle.SUCCESS, label='Confirmer', custom_id=ID_CONFIRMER)
        bouton_annuler = inter.Button(style=inter.ButtonStyle.DANGER, label='Annuler', custom_id=ID_ANNULER)
        row = inter.ActionRow(components=[bouton_confirmer, bouton_annuler])

        async def check(ctx_btn: inter.ComponentContext) -> bool:
            nonlocal a_confirme
            if int(ctx_btn.author.user.id) != int(ctx.author.user.id):
                await ctx.send(embeds=embed_pas_concerne(), ephemeral=True)
                return False
            a_confirme = ctx_btn.data.custom_id == ID_CONFIRMER
            return True
        await ctx.send(embeds=embed_verif, components=row, ephemeral=True)

        try:
            ctx_comp: inter.ComponentContext = await wait_for_component(self.bot,
                                                                        components=[bouton_confirmer, bouton_annuler],
                                                                        check=check, timeout=45)
        except asyncio.TimeoutError:
            await ctx.edit(embeds=embed_temps_ecoule(), components=[])
            if self.logger is not None:
                self.logger.info("Requête inscription expirée.")
            return
        await ctx.edit(embeds=embed_traitement(), components=[])
        if not a_confirme:
            await ctx_comp.send(embeds=embed_annule(), ephemeral=True)
            return
        self.gbd.ajouter_assoiffe(prenom, nom, id_discord, annee, groupe_td, groupe_tp)
        embed_succes = inter.Embed(
            title=f'{EMOJI.name} Succès',
            description=f"{prenom} {nom} a été inscrit dans la base de données.",
            color=inter.Color.green()
        )
        if self.logger is not None:
            self.logger.info(f"Inscription de {prenom} {nom}")
        await ctx_comp.send(embeds=embed_succes)

    @inter.extension_command(
        name='inscription',
        description="Vous inscrit dans la base de données pour que vous puissiez enregistrer des gobelets.",
        options=[
            inter.Option(
                name='prenom',
                description="Votre prénom",
                type=inter.OptionType.STRING,
                required=True
            ),
            inter.Option(
                name='nom',
                description='Votre nom',
                type=inter.OptionType.STRING,
                required=True
            ),
            inter.Option(
                name='annee',
                type=inter.OptionType.INTEGER,
                description="Votre année à l'IUT. Mettez 0 si vous ne faites pas partie du cursus.",
                required=True
            ),
            inter.Option(
                name='groupe_td',
                type=inter.OptionType.INTEGER,
                description="Votre numéro de groupe de TD. Mettez 0 si vous n'êtes pas à l'IUT et 5 si vous êtes en "
                            "alternance.",
                required=True
            ),
            inter.Option(
                name='groupe_tp',
                type=inter.OptionType.INTEGER,
                description="Votre numéro de groupe de TP. Mettez 0 si vous n'êtes pas à l'IUT ou que vous êtes en "
                            "alternance.",
                required=True
            )
        ]
    )
    async def inscription(self, ctx: inter.CommandContext, prenom: str, nom: str,
                          annee: int, groupe_td: int, groupe_tp: int):
        await self.inscription_gens(ctx, prenom, nom, ctx.author.user, annee, groupe_td, groupe_tp)

    @inter.extension_command(
        name='inscription_autrui',
        description="Inscrit quelqu\'un d'autre que soi-même dans la base de données. Pour Lwi seulement.",
        options=[
            inter.Option(
                name='prenom',
                description="Le prénom",
                type=inter.OptionType.STRING,
                required=True
            ),
            inter.Option(
                name='nom',
                description='Le nom',
                type=inter.OptionType.STRING,
                required=True
            ),
            inter.Option(
                name='discord',
                description='La personne à inscrire',
                type=inter.OptionType.USER,
                required=True
            ),
            inter.Option(
                name='annee',
                type=inter.OptionType.INTEGER,
                description="Son année à l'IUT. Mettez 0 si la personne ne fait pas partie du cursus.",
                required=True
            ),
            inter.Option(
                name='groupe_td',
                type=inter.OptionType.INTEGER,
                description="Son numéro de groupe de TD. 5 pour l'alternance, 0 hors de l'IUT.",
                required=True
            ),
            inter.Option(
                name='groupe_tp',
                type=inter.OptionType.INTEGER,
                description="Son numéro de groupe de TP. 0 pour l'alternance et les externes à l'IUT.",
                required=True
            )
        ]
    )
    async def inscription_autrui(self, ctx: inter.CommandContext, prenom: str, nom: str,
                                 discord: inter.User, annee: int, groupe_td: int, groupe_tp: int):
        if int(ctx.author.user.id) != LWI:
            await ctx.send(embeds=embed_pas_lwi(), ephemeral=True)
            return
        await self.inscription_gens(ctx, prenom, nom, discord, annee, groupe_td, groupe_tp)

