import asyncio
import sqlite3

import interactions as inter
from interactions.ext.wait_for import wait_for_component

from locals import embed_pas_lwi, LWI, est_date_valide, embed_erreur_date, embed_pas_inscrit, generer_select_taille, \
    generer_select_personne, ID_ENVOYER, ID_SELECT_PERSONNE, embed_pas_concerne, \
    ID_SELECT_TAILLE, ID_CHOIX_BOISSON, embed_temps_ecoule, embed_traitement, EMOJI, embed_choisir_boisson, \
    embed_choisir_taille, get_nom_taille, ID_CONFIRMER, ID_ANNULER, embed_annule
from squelette import Squelette


class Gobelet(Squelette):

    async def gob(self, ctx: inter.CommandContext, discord: inter.User, date: str):
        if self.logger is not None:
            self.logger.info(f"Appel de la fonction Gobelet sur {discord.username}")
        if not est_date_valide(date):
            await ctx.send(embeds=embed_erreur_date(date), ephemeral=True)
            return

        embed_debut = inter.Embed(
            title=f'{EMOJI.name} Ajout de gobelet',
            color=inter.Color.white(),
            description='Vous pouvez rejeter tous les messages "*Caféinator réfléchit...*"'
        )
        choix_boissons = self.genere_choix_boisson(True)
        choix_taille = generer_select_taille(True)
        choix_personne = generer_select_personne(f'{discord.username}#{discord.discriminator}', True)
        bouton_submit = inter.Button(style=inter.ButtonStyle.PRIMARY, label='Envoyer', custom_id=ID_ENVOYER)
        composants = [[choix_personne], [choix_boissons], [choix_taille], [bouton_submit]]
        composants_plat = [i[0] for i in composants]
        boisson = None
        personne = 1
        taille = None
        submit_clique = False

        async def check(comp_ctx: inter.ComponentContext) -> bool:
            nonlocal boisson, personne, taille, submit_clique
            if int(comp_ctx.author.user.id) != int(ctx.author.user.id):
                await ctx.send(embeds=embed_pas_concerne(), ephemeral=True)
                return False
            if comp_ctx.data.custom_id == ID_ENVOYER:
                if None in (boisson, taille):
                    embed_erreur = embed_choisir_boisson if boisson is None else embed_choisir_taille
                    await comp_ctx.send(embeds=embed_erreur(), ephemeral=True)
                    return False
                submit_clique = True
            if comp_ctx.data.custom_id == ID_SELECT_PERSONNE:
                personne = int(comp_ctx.data.values[0])
            if comp_ctx.data.custom_id == ID_SELECT_TAILLE:
                taille = int(comp_ctx.data.values[0])
            if comp_ctx.data.custom_id == ID_CHOIX_BOISSON:
                boisson = int(comp_ctx.data.values[0])
            return True

        await ctx.send(embeds=embed_debut, components=composants, ephemeral=True)
        while True:
            try:
                ctx_btn: inter.ComponentContext = await wait_for_component(self.bot, components=composants_plat,
                                                                           timeout=45, check=check)
            except asyncio.TimeoutError:
                await ctx.edit(embeds=embed_temps_ecoule(), components=[])
                if self.logger is not None:
                    self.logger.info("Requête gobelet expirée")
                return
            if submit_clique:
                break
            await ctx_btn.defer(ephemeral=True)
        await ctx.edit(embeds=embed_traitement(), components=list())

        taille = bool(taille)
        if boisson == -1:
            boisson = None
            texte_boisson = 'Boisson inconnue'
        else:
            texte_boisson = self.gbd.nom_boisson_par_id(boisson)
        if not personne:
            id_bd = None
            texte_personne = 'Personne inconnue'
        else:
            try:
                id_bd = self.gbd.assoiffe_par_discord(int(discord.id))
                texte_personne = discord.mention
            except ValueError:
                await ctx_btn.send(embeds=embed_pas_inscrit(discord))
                return
        embed_verif = inter.Embed(
            title=f"{EMOJI.name} Validation du gobelet",
            description='Veuillez vérifier si toutes les données sont correctes.',
            color=inter.Color.yellow(),
            fields=[
                inter.EmbedField(
                    name='Nom de l\'assoiffé',
                    value=texte_personne
                ),
                inter.EmbedField(
                    name='Boisson sélectionnée',
                    value=texte_boisson
                ),
                inter.EmbedField(
                    name='Taille du gobelet',
                    value=get_nom_taille(taille).title()
                ),
                inter.EmbedField(
                    name='Date de consommation',
                    value="Aujourd'hui" if not date else date
                )])
        bouton_confirmer = inter.Button(style=inter.ButtonStyle.SUCCESS, label='Confirmer', custom_id=ID_CONFIRMER)
        bouton_annuler = inter.Button(style=inter.ButtonStyle.DANGER, label='Annuler', custom_id=ID_ANNULER)
        a_confirme = False

        async def check_2(context: inter.ComponentContext) -> bool:
            nonlocal a_confirme
            if int(context.author.user.id) != int(ctx.author.user.id):
                await context.send(embeds=embed_pas_concerne(), ephemeral=True)
                return False
            a_confirme = context.data.custom_id == ID_CONFIRMER
            return True

        await ctx_btn.send(embeds=[embed_verif], components=[bouton_confirmer, bouton_annuler], ephemeral=True)
        try:
            ctx_verif: inter.ComponentContext = await wait_for_component(self.bot, timeout=45, check=check_2,
                                                                         components=[bouton_confirmer, bouton_annuler])
        except asyncio.TimeoutError:
            await ctx_btn.edit(embeds=embed_temps_ecoule(), components=[])
            if self.logger is not None:
                self.logger.info("Requête gobelet expirée.")
            return
        # await ctx_btn.edit(embeds=embed_traitement(), components=[])
        if not a_confirme:
            await ctx_verif.send(embeds=embed_annule(), ephemeral=True)
            return
        if self.logger is not None:
            self.logger.info(f'Appel de gob({id_bd}, {boisson}, {taille}, {date}).')
        try:
            self.gbd.ajouter_gobelet(id_bd, boisson, taille, date)
        except sqlite3.IntegrityError:
            embed_trigger = inter.Embed(
                title=f'{EMOJI.name} Erreur : Taille incompatible',
                description='La boisson choisie n\'est pas disponible de la taille indiquée.',
                color=inter.Color.red())
            await ctx_verif.send(embeds=embed_trigger, ephemeral=True)
            return
        embed_succes = inter.Embed(
            title=f'{EMOJI.name} Gobelet ajouté',
            description='Votre gobelet a été inscrit dans la base de données.',
            color=inter.Color.green(),
            fields=embed_verif.fields)
        await ctx_verif.send(embeds=embed_succes)

    @inter.extension_command(
        name='gobelet',
        description='Vous ajoute un gobelet dans la base de données.',
        options=[
            inter.Option(
                name='date',
                description="La date précise au format YYYY-MM-DD, aujourd'hui par défaut",
                type=inter.OptionType.STRING,
                required=False
            )
        ]
    )
    async def gobelet(self, ctx: inter.CommandContext, date: str = ''):
        await self.gob(ctx, ctx.author.user, date)

    @inter.extension_command(
        name='gobelet_autrui',
        description="Met un gobelet à une personne. Pour Lwi uniquement.",
        options=[
            inter.Option(
                name='personne_cible',
                description='La personne qui a bu le gobelet',
                type=inter.OptionType.USER,
                required=True
            ),
            inter.Option(
                name='date',
                description="La date précise au format YYYY-MM-DD, aujourd'hui par défaut",
                type=inter.OptionType.STRING,
                required=False
            )
        ]
    )
    async def gobelet_autrui(self, ctx: inter.CommandContext, personne_cible: inter.User, date: str = ''):
        if int(ctx.author.user.id) != LWI:
            await ctx.send(embeds=embed_pas_lwi(), ephemeral=True)
            return
        await self.gob(ctx, personne_cible, date)
