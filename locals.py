import datetime
from enum import Enum
import interactions as inter
from typing import Optional

LWI = 345940988440739843
ModeConsult = Enum('ModeConsult', ['NOMBRE', 'PRIX', 'MOYENNE', 'RECORD'])

EMOJI = inter.Emoji(name='☕')
DICO_TAILLE = {0: "petit", 1: "grand"}
ID_CHOIX_NOM = 'id_nom'
ID_SELECT_TAILLE = 'selection_taille'
ID_CHOIX_BOISSON = 'choix_boisson'
ID_ENVOYER = 'envoyer'
ID_SELECT_PERSONNE = 'id_personne'
ID_EXEC = 'id_exec'
ID_INPUT_EXEC = 'id_input_exec'
ID_CONFIRMER = 'id_confirmer'
ID_ANNULER = 'id_annuler'


def get_nom_taille(taille: Optional[int]) -> str:
    dico = {0: ' petit', 1: ' grand'}
    return dico.get(taille, '')


def generer_select_personne(nom: str, insertion: bool = False) -> inter.SelectMenu:
    texte = 'Personne inconnue' if insertion else "Tous les assoiffés"
    tout_le_monde = inter.SelectOption(label=texte, value=0)
    personne = inter.SelectOption(label=nom, value=1, default=insertion)
    return inter.SelectMenu(options=[tout_le_monde, personne], custom_id=ID_SELECT_PERSONNE,
                            placeholder="Choisissez la personne")


def generer_select_taille(insertion: bool) -> inter.SelectMenu:
    petit = inter.SelectOption(label='Petit', value=0, default=not insertion)
    grand = inter.SelectOption(label='Grand', value=1, default=not insertion)
    return inter.SelectMenu(options=[petit, grand], custom_id=ID_SELECT_TAILLE, max_values=1 if insertion else 2,
                            placeholder="Choisissez une taille")


def try_catch_date(date: str) -> bool:
    try:
        datetime.date.fromisoformat(date)
    except ValueError:
        return False
    else:
        return True


def est_date_valide(date: str) -> bool:
    if not date:
        return True
    if date[0] in '<>':
        return try_catch_date(date[1:])
    if date.count('/') == 1:
        d1, d2 = date.split('/')
        return try_catch_date(d1) and try_catch_date(d2)
    return try_catch_date(date)


def embed_erreur_date(date: str) -> inter.Embed:
    return inter.Embed(
        title=f"{EMOJI.name} Erreur : Date Invalide",
        color=inter.Color.red(),
        fields=[inter.EmbedField(
            name='Date invalide',
            value=f'La date "{date}" n\'est pas une date ISO valide.'
        )]
    )


def embed_pas_concerne() -> inter.Embed:
    return inter.Embed(
        title=f"{EMOJI.name} Erreur : Mauvaise Personne ",
        color=inter.Color.red(),
        fields=[inter.EmbedField(
            name='Mauvais destinataire',
            value="Vous n'êtes pas ciblé par ce message. Il est destiné à quelqu'un d'autre."
        )]
    )


def embed_choisir_personne() -> inter.Embed:
    return inter.Embed(
        title=f"{EMOJI.name} Erreur : Formulaire incomplet",
        color=inter.Color.red(),
        fields=[inter.EmbedField(
            name="Champ de la personne cible vide",
            value="Vous n'avez pas choisi la cible de votre requête. Veuillez la préciser avant de valider."
        )]
    )


def embed_choisir_taille() -> inter.Embed:
    return inter.Embed(
        title=f"{EMOJI.name} Erreur : Formulaire incomplet",
        color=inter.Color.red(),
        fields=[inter.EmbedField(
            name="Champ de la taille vide",
            value="Vous n'avez pas choisi la taille de votre gobelet. Veuillez la préciser avant de valider."
        )]
    )


def embed_choisir_boisson() -> inter.Embed:
    return inter.Embed(
        title=f"{EMOJI.name} Erreur : Formulaire incomplet",
        color=inter.Color.red(),
        fields=[inter.EmbedField(
            name="Champ de la boisson vide",
            value="Vous n'avez pas choisi votre boisson. Veuillez la préciser avant de valider."
        )]
    )


def embed_traitement() -> inter.Embed:
    return inter.Embed(
        title=f"{EMOJI.name} Traitement en cours",
        color=inter.Color.yellow(),
        fields=[inter.EmbedField(
            name="La requête a été envoyée.",
            value=EMOJI.name
        )]
    )


def embed_temps_ecoule() -> inter.Embed:
    return inter.Embed(
        title=f"{EMOJI.name} Requête expirée",
        color=inter.Color.red(),
        fields=[inter.EmbedField(
            name="Temps de réponse écoulé.",
            value="Vous avez mis trop de temps à répondre. La requête a expiré. Veuillez recommencer plus vite."
        )]
    )


def embed_pas_inscrit(user: inter.User) -> inter.Embed:
    return inter.Embed(
        title=f"{EMOJI.name} Erreur : Utilisateur non identifié",
        color=inter.Color.red(),
        fields=[inter.EmbedField(
            name=f"{user.username} n'existe pas.",
            value=f"{user.mention} n'est pas enregistré dans la base de données. Impossible de donner suite à votre "
                  f"requête. "
        )]
    )


def embed_pas_lwi() -> inter.Embed:
    return inter.Embed(
        title=f"{EMOJI.name} Commande pour Lwi",
        color=inter.Color.red(),
        description="Cette commande n'est utilisable que par Lwi. Pas vous."
    )


def embed_annule() -> inter.Embed:
    return inter.Embed(
        title=f"{EMOJI.name} Annulation de la requête",
        color=inter.Color.red(),
        description="La requête précédente a été annulée."
    )
