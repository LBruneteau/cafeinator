import logging
import os
import dotenv
import interactions as inter
from automate import Automate


def get_token() -> str:
    dotenv.load_dotenv()
    return os.getenv('TOKEN')


def main() -> None:
    logging.basicConfig(filemode='w', filename='logs.txt', level=logging.INFO,
                        # logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s-%(levelname)s-%(message)s')
    bot = inter.Client(token=get_token())
    Automate(bot, logger=logging.getLogger())
    bot.start()


if __name__ == '__main__':
    main()
