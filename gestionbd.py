import zipfile
import sqlite3
import datetime
import logging
from typing import Optional, List, Tuple


def where_gobelet(id_assoiffe: Optional[int] = None, id_boisson: Optional[int] = None,
                  grand: Optional[bool] = None, date: Optional[str] = None) -> Tuple[str, tuple]:
    """
    Crée la clause WHERE d'une requête sql sur la table Gobelet avec les conditions correspondant aux paramètres.
    Place des points d'interrogation à la place des paramètres, qui doivent être bind ailleurs.
    Retourne également la liste des paramètres
    :param id_assoiffe : l'id de l'assoiffé demandé, tous si None
    :param id_boisson : l'id de la boisson demandée, tous si None
    :param grand : la taille de la boisson, tous si None
    :param date : Peut être supérieure, inférieure à une date ou comprise entre deux avec un /
    :return : La clause WHERE formatée et un tuple de paramètres
    """
    requete: str = ''
    args = list()
    if id_assoiffe is not None:
        requete += "WHERE id_assoiffe = ? "
        args.append(id_assoiffe)
    if id_boisson is not None:
        requete += "WHERE " if not requete else "AND "
        requete += 'id_boisson = ? '
        args.append(id_boisson)
    if grand is not None:
        requete += "WHERE " if not requete else "AND "
        requete += 'taille = ? '
        args.append(int(grand))
    if date:
        requete += "WHERE " if not requete else "AND "
        if '/' in date:
            requete += 'date >= ? AND date < ? '
            date1, date2 = date.split('/')
            args.append(date1)
            args.append(date2)
        else:
            operateur = date[0] if date[0] in '<>' else '='
            requete += f'date {operateur} ? '
            args.append(date[1:])
    return requete, tuple(args)


class GestionBd:

    def __init__(self, chemin_bd: str, logger: Optional[logging.Logger] = None):
        self.chemin_bd: str = chemin_bd
        self.conn = sqlite3.connect(chemin_bd)
        self.cur = self.conn.cursor()
        self.logger: Optional[logging.Logger] = logger

    def creer_backup(self):
        date = datetime.date.today()
        nom = f'backup/{date.isoformat()}.zip'
        with zipfile.ZipFile(nom, "w") as archive:
            archive.write(self.chemin_bd)
        if self.logger is not None:
            self.logger.info(f"Backup {nom} créée.")

    def executer_requete(self, requete: str, *args) -> sqlite3.Cursor:
        res = self.cur.execute(requete, *args)
        self.conn.commit()
        if self.logger is not None:
            self.logger.info(f'Requête {requete} exécutée avec les arguments {args}.')
        return res

    def assoiffe_par_discord(self, discord: int) -> int:
        """
        Retourne l'id d'un assoiffé à partir de son identifiant discord
        :param discord : L'identifiant discord d'un assoiffé
        :return : L'identifiant unique d'un assoiffé dans la base de données
        """
        liste_id = self.executer_requete("SELECT id FROM Assoiffe WHERE discord=?;", (discord,)).fetchall()
        if not liste_id:
            if self.logger is not None:
                self.logger.info(f"Tentative de trouver l'identifiant associé au discord {discord} échouée.")
            raise ValueError('Utilisateur non trouvé')
        if self.logger is not None:
            self.logger.info(f"Discord {discord} associé à l'assoiffé {liste_id[0][0]}.")
        return liste_id[0][0]

    def assoiffe_par_nom(self, prenom: str, nom: str) -> int:
        """
        Retourne l'id d'un assoiffé dans la base de données à partir de son prénom et de son nom.
        :param prenom : Le prénom de la personne
        :param nom : Le nom de la personne
        :return : l'ID de la personne
        """
        prenom, nom = prenom.title(), nom.title()
        liste_id = self.executer_requete("SELECT id FROM Assoiffe WHERE prenom=? AND nom=?;", (prenom, nom)).fetchall()
        if not liste_id:
            if self.logger is not None:
                self.logger.info(f"Tentative de trouver l'assoiffé {prenom} {nom.upper()} échouée")
            raise ValueError('Utilisateur non trouvé')
        if self.logger is not None:
            self.logger.info(f"{prenom} {nom.upper()} associé à l'assoiffé {liste_id[0][0]}.")
        return liste_id[0][0]

    def boisson_par_nom(self, nom: str) -> int:
        """
        Retourne l'ID d'une boisson dans la BD à partir de son nom.
        :param nom : Le nom unique de la boisson
        :return : l'id de la boisson
        """
        nom = nom.title()
        liste_id = self.executer_requete("SELECT id FROM Boisson WHERE nom=?", (nom,)).fetchall()
        if not liste_id:
            if self.logger is not None:
                self.logger.info(f"Boisson {nom} non trouvée.")
            raise ValueError("Boisson non trouvée")
        if self.logger is not None:
            self.logger.info(f"Boisson {nom} associée à l'identifiant {liste_id[0][0]}.")
        return liste_id[0][0]

    def nom_boisson_par_id(self, identifiant: Optional[int]) -> str:
        liste = self.executer_requete("SELECT nom FROM boisson WHERE id=?", (identifiant,)).fetchall()
        if not liste:
            raise ValueError("Boisson non trouvée")
        return liste[0][0]

    def ajouter_assoiffe(self, prenom: str, nom: str, discord: int, annee: Optional[int], groupe_td: Optional[int],
                         groupe_tp: Optional[int]) -> None:
        requete = "INSERT INTO Assoiffe (prenom, nom, discord, annee, groupe_td, groupe_tp) VALUES (?, ?, ?, ?, ?, ?)"
        if self.logger is not None:
            self.logger.info(f"Tentative d'ajout de l'assoiffé {prenom.title()} {nom.upper()}.")
        self.executer_requete(requete, (prenom.title(), nom.title(), discord, annee, groupe_td, groupe_tp))

    def ajouter_gobelet(self, id_assoiffe: Optional[int], id_boisson: Optional[int], grand: bool = False,
                        date: Optional[str] = '') -> None:
        """
        Ajoute un gobelet dans la BD
        :param id_assoiffe : L'id de l'assoiffé, None si inconnu
        :param id_boisson : L'id de la boisson, None si inconnue
        :param grand : La taille du gobelet
        :param date : La date de consommation. None si inconnue, chaîne de caractères vide pour aujourd'hui
        """
        if date == '' and date is not None:
            date = datetime.date.today().isoformat()
        requete = "INSERT INTO Gobelet (id_assoiffe, id_boisson, taille, date) VALUES (?, ?, ?, ?)"
        self.executer_requete(requete, (id_assoiffe, id_boisson, int(grand), date))

    def nb_gobelets(self, id_assoiffe: Optional[int] = None, id_boisson: Optional[int] = None,
                    grand: Optional[bool] = None, date: str = "") -> int:
        """
        Retourne le nombre de gobelets bus selon un ensemble de critères
        :param id_assoiffe : La personne qui consomme les boissons, None pour tout le monde
        :param id_boisson : La boisson en particulier, None pour toutes
        :param grand : La taille de la boisson, None pour toutes
        :param date : La date, vide pour aucune
        :return:
        """
        requete = "SELECT COUNT(*) FROM Gobelet "
        where, args = where_gobelet(id_assoiffe, id_boisson, grand, date)
        return self.executer_requete(requete + where, args).fetchall()[0][0]

    def liste_boissons(self) -> List[tuple]:
        """
        Retourne la liste des boissons et tous leurs attributs (id, nom, categorie, prix_petit, prix_grand)
        :return : La liste des tuples qui représentent la boisson
        """
        return self.executer_requete("SELECT * FROM Boisson").fetchall()

    def record(self, id_assoiffe: Optional[int] = None, id_boisson: Optional[int] = None,
               grand: Optional[bool] = None, date: str = "") -> Tuple[str, int]:
        """
        Retourne le record du plus de gobelets en une journée
        :param id_assoiffe : Un buveur particulier
        :param id_boisson : Une boisson spécifique
        :param grand : La taille du gobelet
        :param date : La date de la boisson
        :return : La date au format ISO du record et le nombre de gobelets bus
        """
        requete = "SELECT date, COUNT(*) FROM Gobelet WHERE DATE IS NOT NULL "
        where, args = where_gobelet(id_assoiffe, id_boisson, grand, date)
        where = where.replace("WHERE", 'AND')
        requete += where + 'GROUP BY date ORDER BY 2 DESC LIMIT 1'
        return self.executer_requete(requete, args).fetchall()[0]

    def moyenne(self, id_assoiffe: Optional[int] = None, id_boisson: Optional[int] = None,
                grand: Optional[bool] = None, date: str = "") -> int:
        """
        Retourne la moyenne de gobelets récupérés par jour selon les critères en paramètres
        :param id_assoiffe : Un buveur particulier
        :param id_boisson : Une boisson spécifique
        :param grand : La taille du gobelet
        :param date : La date de la boisson
        :return : Le nombre de gobelets par jour
        """
        where, args = where_gobelet(id_assoiffe, id_boisson, grand, date)
        where = where.replace('WHERE', 'AND')
        requete = f"SELECT AVG(nb) FROM (SELECT COUNT(*) AS nb FROM Gobelet " \
                  f"WHERE date IS NOT NULL {where} GROUP BY date) as X"
        return self.executer_requete(requete, args).fetchall()[0][0]

    def prix_paye(self, id_assoiffe: Optional[int] = None, id_boisson: Optional[int] = None,
                  grand: Optional[bool] = None, date: str = "") -> float:
        """
        Retourne la somme d'argent dépensée
        :param id_assoiffe : Un buveur particulier
        :param id_boisson : Une boisson spécifique
        :param grand : La taille du gobelet
        :param date : La date de la boisson
        :return : La somme dépensée en euro
        """
        where, args = where_gobelet(id_assoiffe, id_boisson, grand, date)
        requete = "SELECT SUM(CASE WHEN taille = 1 THEN prix_grand WHEN taille=0 THEN prix_petit END)" \
                  f" FROM Gobelet INNER JOIN Boisson ON Boisson.id = id_boisson {where}"
        return self.executer_requete(requete, args).fetchall()[0][0] / 100

    def classement_boissons(self, id_assoiffe: Optional[int] = None, grand: Optional[bool] = None,
                            date: str = "") -> List[Tuple[str, int]]:
        """
        Retourne un classement des boissons en fonction de leur nombre de gobelets associé
        :param id_assoiffe : Un buveur particulier
        :param grand : La taille des gobelets
        :param date : La date précise
        :return : Le nom de la boisson et le nombre de gobelets associés
        """
        where, args = where_gobelet(id_assoiffe, None, grand, date)
        requete = "SELECT nom, COUNT(*) FROM Gobelet INNER JOIN Boisson on Boisson.id = id_boisson " \
                  f"{where} GROUP BY (nom) ORDER BY 2 DESC"
        return self.executer_requete(requete, args).fetchall()

    def classement_assoiffes(self, id_boisson: Optional[int] = None,
                             grand: Optional[bool] = None, date: str = "") -> List[Tuple[int, int]]:
        """
        Retourne un classement des assoiffés qui ont bu le plus de café
        :param id_boisson : Une boisson spécifique
        :param grand : Une taille précise
        :param date : Une date (bien vu l'aveugle)
        :return : L'identifiant et le nombre de gobelets consommés
        """
        where, args = where_gobelet(None, id_boisson, grand, date)
        requete = "SELECT Assoiffe.id, COUNT(*) FROM Gobelet INNER JOIN Assoiffe ON Assoiffe.id = id_assoiffe " \
                  f"{where} GROUP BY (Assoiffe.id) ORDER BY 2 DESC"
        return self.executer_requete(requete, args).fetchall()

    def classement_assoiffes_prix(self, id_boisson: Optional[int] = None,
                                  grand: Optional[bool] = None, date: str = "") -> List[Tuple[int, float]]:
        """
        Retourne un classement des assoiffés qui ont dépensé le plus de sous à la machine à café
        :param id_boisson : Une boisson spécifique
        :param grand : Une taille précise
        :param date : Une date (bien vu l'aveugle)
        :return : L'identifiant et l'argent dépensé
        """
        where, args = where_gobelet(None, id_boisson, grand, date)
        requete = "SELECT Assoiffe.id, SUM(CASE WHEN taille = 1 THEN prix_grand WHEN taille=0 THEN prix_petit END)" \
                  " FROM Gobelet INNER JOIN Assoiffe ON Assoiffe.id = id_assoiffe" \
                  " INNER JOIN Boisson ON Boisson.id = id_boisson " \
                  f"{where} GROUP BY (Assoiffe.id) ORDER BY 2 DESC"
        res = self.executer_requete(requete, args).fetchall()
        return [(i, prix/100) for i, prix in res]

    def discord_par_id(self, identifiant: int) -> Optional[int]:
        """
        Retourne l'identifiant discord d'un assoiffé à partir de son id
        :param identifiant : l'identifiant dans la base de données
        :return : Un id discord ou None
        """
        res = self.executer_requete("SELECT discord FROM Assoiffe WHERE id = ?", (identifiant,)).fetchall()
        if not res:
            raise ValueError('Cet identifiant n\'est pas associé à un assoiffé ou à un discord.')
        return res[0][0]

    def liste_noms_id(self) -> List[Tuple[int, str, str]]:
        """
        Retourne les noms de tous les assoiffés de la bd
        :return : une liste de tuple id + prenom + nom
        """
        return self.executer_requete("SELECT id, prenom, nom FROM Assoiffe").fetchall()

    def supprimer_dernier(self, id_assoiffe: Optional[int] = None, id_boisson: Optional[int] = None,
                          grand: Optional[bool] = None, date: Optional[str] = '') -> None:
        where, args = where_gobelet(id_assoiffe, id_boisson, grand, date)
        requete = f"DELETE FROM Gobelet AS go WHERE go.id = (SELECT MAX(id) FROM Gobelet {where})"
        self.executer_requete(requete, args)
