"""Convertit l'ancien modèle de BD vers le nouveau.
S'exécute sur une bd où creer_bd.sql peupler_assoiffes.sql et peupler_boissons.sql ont été exécutés.
"""

import sqlite3
from typing import Optional


def get_boisson(ancien_id: int) -> Optional[int]:
    """
    Retourne le nouvel id d'une boisson à partir de l'ancien
    :param ancien_id : L'id de la boisson dans l'ancienne base de données
    :return : L'id de la bd dans la nouvelle ou None pour une boisson inconnue
    """
    if ancien_id in range(2, 23):
        return ancien_id - 1
    return None


def get_assoiffe(ancien_id: int) -> Optional[int]:
    """
    Associe l'id d'un assoiffé de l'ancienne bd vers la nouvelle.
    :param ancien_id : L'id de l'assoiffé dans l'ancienne bd
    :return : l'id de l'assoiffé dans la nouvelle bd
    """
    if ancien_id <= 1 or ancien_id == 19:
        return None
    if ancien_id <= 18:
        return ancien_id - 1
    if ancien_id <= 48:
        return ancien_id - 2
    return None


def insertion_gobelet(curseur: sqlite3.Cursor, date: str, id_assoiffe: Optional[int],
                      id_boisson: Optional[int], taille: int):
    if date <= '2021-11-01':
        date = None
    id_assoiffe = get_assoiffe(id_assoiffe)
    id_boisson = get_boisson(id_boisson)
    if id_assoiffe == 36:  # Supprimer la date des gobelets d'Elliot
        date = None
    curseur.execute("INSERT INTO Gobelet(date, id_assoiffe, id_boisson, taille) VALUES (?, ?, ?, ?);",
                    (date, id_assoiffe, id_boisson, taille))


def main():
    input("Vous vous apprêtez à écrire sur la base de données. Faites Ctrl C pour arrêter.")
    ancienne_conn = sqlite3.connect("ancienne.db")
    nouvelle_conn = sqlite3.connect("donnees_gros.db")
    ancien_curseur = ancienne_conn.cursor()
    nouveau_curseur = nouvelle_conn.cursor()
    anciens_gob = ancien_curseur.execute("SELECT date, id_assoiffé, id_boisson, taille FROM Gobelet;").fetchall()
    for gob in anciens_gob:
        insertion_gobelet(nouveau_curseur, *gob)
    nouvelle_conn.commit()


if __name__ == '__main__':
    main()
