BEGIN TRANSACTION;

INSERT INTO "Boisson" (nom, categorie, prix_petit, prix_grand) VALUES("Ristretto", "Cafés En Grain", 50, NULL);
INSERT INTO "Boisson" (nom, categorie, prix_petit, prix_grand) VALUES("Expresso", "Cafés En Grain", 50, NULL);
INSERT INTO "Boisson" (nom, categorie, prix_petit, prix_grand) VALUES("Café Long", "Cafés En Grain", 50, NULL);

INSERT INTO "Boisson" (nom, categorie, prix_petit, prix_grand) VALUES("Café Court Instantané", "Cafés Instantanés", 40, NULL);
INSERT INTO "Boisson" (nom, categorie, prix_petit, prix_grand) VALUES("Café Long Instantané", "Cafés Instantanés", 40, NULL);
INSERT INTO "Boisson" (nom, categorie, prix_petit, prix_grand) VALUES("Macchiato Instantané", "Cafés Instantanés", 50, NULL);
INSERT INTO "Boisson" (nom, categorie, prix_petit, prix_grand) VALUES("Café Au Lait Instantané", "Cafés Instantanés", 50, NULL);
INSERT INTO "Boisson" (nom, categorie, prix_petit, prix_grand) VALUES("Café Américain Xl Instantané", "Cafés Instantanés", NULL, 70);

INSERT INTO "Boisson" (nom, categorie, prix_petit, prix_grand) VALUES("Cappuccino Instantané", "Boissons Gourmandes", 70, 100);
INSERT INTO "Boisson" (nom, categorie, prix_petit, prix_grand) VALUES("Viennois Instantané", "Boissons Gourmandes", 70, 100);
INSERT INTO "Boisson" (nom, categorie, prix_petit, prix_grand) VALUES("Moccaccino Instantané", "Boissons Gourmandes", 70, 100);
INSERT INTO "Boisson" (nom, categorie, prix_petit, prix_grand) VALUES("Café Caramel", "Boissons Gourmandes", 70, 100);
INSERT INTO "Boisson" (nom, categorie, prix_petit, prix_grand) VALUES("Café Vanille", "Boissons Gourmandes", 70, 100);
INSERT INTO "Boisson" (nom, categorie, prix_petit, prix_grand) VALUES("Café Crème Brûlée", "Boissons Gourmandes", 70, 100);

INSERT INTO "Boisson" (nom, categorie, prix_petit, prix_grand) VALUES("Chocolat", "Boissons Chocolatées", 50, 100);
INSERT INTO "Boisson" (nom, categorie, prix_petit, prix_grand) VALUES("Chocolat Au Lait", "Boissons Chocolatées", 50, 100);
INSERT INTO "Boisson" (nom, categorie, prix_petit, prix_grand) VALUES("Chocolat Fort", "Boissons Chocolatées", 70, 100);
INSERT INTO "Boisson" (nom, categorie, prix_petit, prix_grand) VALUES("Chocolat Caramel", "Boissons Chocolatées", 70, 100);
INSERT INTO "Boisson" (nom, categorie, prix_petit, prix_grand) VALUES("Chocolat Vanille", "Boissons Chocolatées", 70, 100);
INSERT INTO "Boisson" (nom, categorie, prix_petit, prix_grand) VALUES("Chocolat Menthe", "Boissons Chocolatées", 70, 100);

INSERT INTO "Boisson" (nom, categorie, prix_petit, prix_grand) VALUES("Thé Menthe", "Carte des Thés", 50, 100);
--TODO vérifier les prix à la machine. Ajouter les boissons de la machine de la salle polyvalente