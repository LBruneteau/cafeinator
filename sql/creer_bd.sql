CREATE TABLE "Boisson" (
	"id"	INTEGER NOT NULL UNIQUE,
	"nom"	TEXT NOT NULL UNIQUE,
	"categorie"	TEXT,
	"prix_petit"	INTEGER,
	"prix_grand"	INTEGER,
	PRIMARY KEY("id" AUTOINCREMENT),
	CHECK (prix_petit IS NOT NULL OR prix_grand IS NOT NULL)
);


CREATE TABLE "Assoiffe" (
	"id"	INTEGER NOT NULL UNIQUE,
	"prenom" TEXT NOT NULL,
	"nom"	TEXT NOT NULL,
	"discord"	INTEGER UNIQUE,
	"annee" INTEGER,
	"groupe_td"	INTEGER,
	"groupe_tp"	INTEGER,
	PRIMARY KEY("id" AUTOINCREMENT),
	UNIQUE(prenom, nom)
    );

CREATE TABLE "Gobelet" (
	"id"	INTEGER NOT NULL UNIQUE,
	"date"	TEXT,
	"id_assoiffe"	INTEGER,
	"id_boisson"	INTEGER,
	"taille"	BOOLEAN NOT NULL CHECK(taille IN (0, 1)),
	PRIMARY KEY("id" AUTOINCREMENT),
	FOREIGN KEY("id_boisson") REFERENCES "Boissons"("id"),
	FOREIGN KEY("id_assoiffe") REFERENCES "Assoiffe"("id")
    );

CREATE TRIGGER gobelet_mauvaise_taille_insert
AFTER INSERT ON Gobelet
BEGIN SELECT CASE
WHEN (NEW.taille = 0 AND NEW.id_boisson IN
(SELECT b.id FROM Boisson b WHERE b.prix_petit IS NULL))
OR (NEW.taille = 1 AND NEW.id_boisson IN
(SELECT b.id FROM Boisson b WHERE b.prix_grand IS NULL))
    THEN RAISE(ABORT, 'La taille du gobelet ne correspond pas à la boisson.')
END;
END;

CREATE TRIGGER gobelet_mauvaise_taille_update
AFTER UPDATE OF taille, id_boisson ON Gobelet
BEGIN SELECT CASE
WHEN (NEW.taille = 0 AND NEW.id_boisson IN
(SELECT b.id FROM Boisson b WHERE b.prix_petit IS NULL))
OR (NEW.taille = 1 AND NEW.id_boisson IN
(SELECT b.id FROM Boisson b WHERE b.prix_grand IS NULL))
    THEN RAISE(ABORT, 'La taille du gobelet ne correspond pas à la boisson.')
END;
END;