from interactions.ext.wait_for import wait_for_component
from asyncio import TimeoutError
from typing import Optional

import interactions as inter
from locals import generer_select_personne, ID_SELECT_PERSONNE, ID_ENVOYER, ID_CHOIX_BOISSON, ID_SELECT_TAILLE, \
    generer_select_taille, EMOJI, est_date_valide, embed_erreur_date, embed_pas_concerne, embed_choisir_personne, \
    embed_traitement, embed_temps_ecoule, embed_pas_inscrit, get_nom_taille, ModeConsult
from squelette import Squelette


def embed_formulaire(mode: ModeConsult) -> inter.Embed:
    titre = {
        ModeConsult.NOMBRE: 'Nombre de gobelets',
        ModeConsult.PRIX: 'Somme dépensée',
        ModeConsult.MOYENNE: 'Nombre de gobelets moyen',
        ModeConsult.RECORD: 'Record de gobelets'
    }
    return inter.Embed(
        title=f"{EMOJI.name} {titre[mode]}",
        color=inter.Color.white(),
        description='Vous pouvez rejeter tous les messages "*Caféinator réfléchit...*"'
    )


class Consultation(Squelette):

    async def consult(self, ctx: inter.CommandContext, personne_cible: Optional[inter.User],
                      date: str, mode: ModeConsult):
        nom_fonc_dict = {
            ModeConsult.NOMBRE: 'Nombre',
            ModeConsult.PRIX: 'Prix',
            ModeConsult.MOYENNE: 'Moyenne',
            ModeConsult.RECORD: 'Record'
        }
        nom_fonc = nom_fonc_dict[mode]
        if self.logger is not None:
            self.logger.info(f"{ctx.author.user.username} appelle {nom_fonc}.")
        if not est_date_valide(date):
            await ctx.send(embeds=embed_erreur_date(date), ephemeral=True)
            return

        utilisateur: inter.User = personne_cible if personne_cible is not None else ctx.author.user
        choix_personne = generer_select_personne(f'{utilisateur.username}#{utilisateur.discriminator}')
        choix_boisson = self.genere_choix_boisson(False)
        choix_taille = generer_select_taille(False)
        bouton_submit = inter.Button(style=inter.ButtonStyle.PRIMARY, label='Envoyer', custom_id=ID_ENVOYER)
        id_boisson = None
        taille = None
        submit_clique = False
        personne_choisie = -1
        liste_composants = [[choix_personne], [choix_boisson], [choix_taille], [bouton_submit]]

        async def check(comp_ctx: inter.ComponentContext) -> bool:
            nonlocal submit_clique, id_boisson, taille, personne_choisie
            if int(comp_ctx.author.user.id) != int(ctx.author.user.id):
                await ctx.send(embeds=embed_pas_concerne(), ephemeral=True)
                return False
            if comp_ctx.data.custom_id == ID_SELECT_PERSONNE:
                personne_choisie = int(comp_ctx.data.values[0])
            if comp_ctx.data.custom_id == ID_ENVOYER:
                if personne_choisie == -1:
                    await comp_ctx.send(embeds=embed_choisir_personne(), ephemeral=True)
                    return False
                submit_clique = True
            if comp_ctx.data.custom_id == ID_CHOIX_BOISSON:
                id_boisson = None if int(comp_ctx.data.values[0]) <= 0 else int(comp_ctx.data.values[0])
            elif comp_ctx.data.custom_id == ID_SELECT_TAILLE:
                taille = int(comp_ctx.data.values[0]) if len(comp_ctx.data.values) == 1 else None
            return True

        await ctx.send(embeds=embed_formulaire(mode), components=liste_composants, ephemeral=True)

        while True:
            try:
                ctx_btn: inter.ComponentContext = await wait_for_component(self.bot,
                                                                           components=[choix_boisson, choix_taille,
                                                                                       bouton_submit, choix_personne],
                                                                           check=check, timeout=45)
            except TimeoutError:
                await ctx.edit(embeds=embed_temps_ecoule(), components=[])
                if self.logger is not None:
                    self.logger.info(f"Requête {nom_fonc} expirée")
                return
            else:
                if submit_clique:
                    break
                await ctx_btn.defer(ephemeral=True)
        await ctx.edit(embeds=embed_traitement(), components=list())
        id_bd = None
        if personne_choisie:
            try:
                id_bd = self.gbd.assoiffe_par_discord(int(utilisateur.id))
            except ValueError:
                await ctx_btn.send(embeds=embed_pas_inscrit(utilisateur))
                return
        if self.logger is not None:
            self.logger.info(f"Appel de {nom_fonc}({id_bd}, {id_boisson}, {taille}, '{date}')")
        if mode == ModeConsult.NOMBRE:
            nombre = self.gbd.nb_gobelets(id_bd, id_boisson, taille, date)
        elif mode == ModeConsult.PRIX:
            nombre = self.gbd.prix_paye(id_bd, id_boisson, taille, date)
        elif mode == ModeConsult.RECORD:
            nombre = self.gbd.record(id_bd, id_boisson, taille, date)
        else:
            nombre = self.gbd.moyenne(id_bd, id_boisson, taille, date)

        if self.logger is not None:
            self.logger.info(f"{nom_fonc} retourne {nombre}")
        if mode == ModeConsult.NOMBRE:
            titre = f"{EMOJI.name} {nombre} gobelet{'s' if nombre != 1 else ''}"
        elif mode == ModeConsult.PRIX:
            titre = f"{EMOJI.name} {nombre:.2f}€ dépensé{'s' if nombre != 1 else ''}"
        elif mode == ModeConsult.MOYENNE:
            titre = f"{EMOJI.name} {nombre:.2f} gobelet{'s' if nombre != 1 else ''} par jour"
        else:
            titre = f"{EMOJI.name} Record de {nombre[1]} gobelet{'s' if nombre != 1 else ''} le {nombre[0]}"

        embed_final = inter.Embed(
            title=titre,
            color=inter.Color.green(),
            fields=[
                inter.EmbedField(
                    name="Personne ciblée",
                    value="Tous les assoiffés" if id_bd is None else utilisateur.mention),
                inter.EmbedField(
                    name="Boisson choisie",
                    value="Toutes les boissons" if id_boisson is None else self.gbd.nom_boisson_par_id(id_boisson)),
                inter.EmbedField(
                    name="Taille des gobelets",
                    value=str(get_nom_taille(taille)).title() if taille is not None else 'Toutes'
                ),
                inter.EmbedField(
                    name="Date spécifiée",
                    value=date if date else 'Aucune'
                )
            ]
        )
        await ctx_btn.send(embeds=embed_final)

    @inter.extension_command(name='nombre',
                             description='Indique le nombre de gobelets fournis selon un ensemble de conditions.',
                             options=[
                                 inter.Option(
                                     name="personne_cible",
                                     description="La personne dont vous voulez compter les gobelets.",
                                     type=inter.OptionType.USER,
                                     required=False),
                                 inter.Option(
                                     name='date',
                                     description='La date précise au format YYYY-MM-DD, supporte >, < et /',
                                     type=inter.OptionType.STRING,
                                     required=False
                                 )
                             ]
                             )
    async def nombre(self, ctx: inter.CommandContext, personne_cible: Optional[inter.User] = None, date: str = ''):
        await self.consult(ctx, personne_cible, date, ModeConsult.NOMBRE)

    @inter.extension_command(name='prix',
                             description='Indique le prix payé selon un ensemble de conditions.',
                             options=[
                                 inter.Option(
                                     name="personne_cible",
                                     description="La personne dont vous voulez calculer les dépenses.",
                                     type=inter.OptionType.USER,
                                     required=False),
                                 inter.Option(
                                     name='date',
                                     description='La date précise au format YYYY-MM-DD, supporte >, < et /',
                                     type=inter.OptionType.STRING,
                                     required=False
                                 )
                             ]
                             )
    async def prix(self, ctx: inter.CommandContext, personne_cible: Optional[inter.User] = None, date: str = ''):
        await self.consult(ctx, personne_cible, date, ModeConsult.PRIX)

    @inter.extension_command(name='moyenne',
                             description='Indique le nombre moyen de gobelets donnés selon un ensemble de conditions.',
                             options=[
                                 inter.Option(
                                     name="personne_cible",
                                     description="La personne dont vous voulez calculer la moyenne.",
                                     type=inter.OptionType.USER,
                                     required=False),
                                 inter.Option(
                                     name='date',
                                     description='La date précise au format YYYY-MM-DD, supporte >, < et /',
                                     type=inter.OptionType.STRING,
                                     required=False
                                 )
                             ]
                             )
    async def moyenne(self, ctx: inter.CommandContext, personne_cible: Optional[inter.User] = None, date: str = ''):
        await self.consult(ctx, personne_cible, date, ModeConsult.MOYENNE)

    @inter.extension_command(name='record',
                             description='Indique le plus grand nombre de gobelets selon un ensemble de conditions.',
                             options=[
                                 inter.Option(
                                     name="personne_cible",
                                     description="La personne dont vous voulez calculer le record.",
                                     type=inter.OptionType.USER,
                                     required=False),
                                 inter.Option(
                                     name='date',
                                     description='La date précise au format YYYY-MM-DD, supporte >, < et /',
                                     type=inter.OptionType.STRING,
                                     required=False
                                 )
                             ]
                             )
    async def record(self, ctx: inter.CommandContext, personne_cible: Optional[inter.User] = None, date: str = ''):
        await self.consult(ctx, personne_cible, date, ModeConsult.RECORD)
